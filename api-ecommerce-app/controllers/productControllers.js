const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.addProduct = (reqBody) =>{
	let newProduct = new Product({
		image: reqBody.image,
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	})
	return newProduct.save().then((product, error)=>{

		if(error){
			return false;
		}

		else{
			return true;
		}

	})
}

// Retrieve all products
/*
	Step:
	1. Retrieve all the products from the database

*/
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => result);
}


// Retrieve all Active Products
/*
	Step:
	1. Retrieve all the products from the database
*/
module.exports.getAllActiveProducts = () =>{
	return Product.find({isActive:true}).then(result  => result);
}

// Retrieving a specific product
/*
	Step:
	1. Retrieve the product that matches the product ID provided from the URL.
*/
module.exports.getProduct = (productId) =>{
	return Product.findById(productId).then(result => result);
}

// Update a product
/*
	Steps:
	1. Create a variable "updatedProduct" which will contain the information retrieved from the request body.
	2. Find and update the product using the productId retrieved from request params/url property and the variable "updatedProduct" containing  the information from the request body.
*/
module.exports.updateProduct = (productId, reqBody) =>{
	// Specify the fields/properties to be updated
	let updatedProduct = {
		image: reqBody.image,
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	}

	// Syntax: findByIdAndUpdate(documentId, updatesToBeApplied)
	return Product.findByIdAndUpdate(productId, updatedProduct).then((ProductUpdate, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

//Archiving a product
module.exports.archiveProduct = (productId, reqBody) =>{
	let updateActiveField = {
		isActive : reqBody.isActive
	}

	return Product.findByIdAndUpdate(productId, updateActiveField).then((isActive, error) =>{
		// Product is not archived
		if(error){
			return false;
		}
		// Product archived successfully
		else{
			return true
		}
	})
}